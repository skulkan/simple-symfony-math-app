<?php

namespace AppBundle\Math;

use AppBundle\Math\Exception\ParameterAlreadyProvided;
use AppBundle\Math\Exception\ParameterNotProvided;

class Parameters
{
    /** @var number[] */
    private $parameters = [];

    /**
     * @param string $name
     * @param number $value
     */
    public function addParameter($name, $value)
    {
        if (isset($this->parameters[$name])) {
            throw new ParameterAlreadyProvided(sprintf(
                'Parameter already provided %s, old value %s new value %s',
                $name,
                $this->parameters[$name],
                $value
            ));
        }

        $this->parameters[$name] = $value;
    }

    /**
     * @param string $name
     * @return number
     */
    public function getParameter($name)
    {
        if (!isset($this->parameters[$name])) {
            throw new ParameterNotProvided(sprintf(
                'Missing required param %s',
                $name
            ));
        }

        return $this->parameters[$name];
    }

    /**
     * @param \ArrayIterator $data
     */
    public function fromIterator(\ArrayIterator $data)
    {
        foreach ($data as $key => $value) {
            $this->addParameter($key, $value);
        }
    }
}
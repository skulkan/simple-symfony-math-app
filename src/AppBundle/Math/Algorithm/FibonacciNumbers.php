<?php

namespace AppBundle\Math\Algorithm;

use AppBundle\Math\Parameters;

class FibonacciNumbers implements AlgorithmInterface
{
    /**
     * @param Parameters $params
     * @return number[]
     */
    public function compute(Parameters $params)
    {
        $untilReach = $params->getParameter('until_reach');

        if ($untilReach <= 0) {
            return [0];
        }

        if ($untilReach <= 1) {
            return [0, 1];
        }

        $result = [0, 1];

        while ($result[count($result)-1] < $untilReach) {
            $result[] = $result[count($result)-1] + $result[count($result)-2];
        }

        return $result;
    }
}
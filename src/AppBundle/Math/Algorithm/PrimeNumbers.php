<?php

namespace AppBundle\Math\Algorithm;

use AppBundle\Math\Parameters;

class PrimeNumbers implements AlgorithmInterface
{
    /**
     * @param Parameters $params
     * @return number[]
     */
    public function compute(Parameters $params)
    {
        $untilReach = $params->getParameter('until_reach');

        if ($untilReach <= 0) {
            return [];
        }

        $result = [];

        for ($i = 2; $i <= $untilReach; $i++) {
            $isPrime = true;
            for ($j = 2; $j < $i; $j++) {
                if ($i % $j == 0) {
                    $isPrime = false;
                }
            }
            if ($isPrime) {
                $result[] = $i;
            }
        }

        return $result;
    }
}
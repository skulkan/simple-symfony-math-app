<?php

namespace AppBundle\Math\Algorithm;

use AppBundle\Math\Parameters;

interface AlgorithmInterface
{
    /**
     * @param Parameters $params
     * @return number[]
     */
    public function compute(Parameters $params);
}
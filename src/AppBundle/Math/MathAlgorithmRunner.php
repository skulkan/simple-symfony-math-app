<?php

namespace AppBundle\Math;

use AppBundle\Math\Algorithm\AlgorithmInterface;
use AppBundle\Math\Exception\AlgorithmNotExists;
use AppBundle\Math\Exception\AlgorithmInterfaceExpected;

class MathAlgorithmRunner
{
    /** @var AlgorithmInterface[] */
    private $algorithmsList;

    /**
     * MathAlgorithmRunner constructor.
     * @param AlgorithmInterface[] $algorithmsList
     */
    public function __construct(array $algorithmsList)
    {
        foreach ($algorithmsList as $algorithm) {
            if (!$algorithm instanceof AlgorithmInterface) {
                throw new AlgorithmInterfaceExpected(
                    'Expected AlgorithmInterface instance'
                );
            }
        }
        
        $this->algorithmsList = $algorithmsList;
    }

    /**
     * @param string $algorithmName
     * @param Parameters $params
     * @return number[]
     */
    public function run($algorithmName, Parameters $params)
    {
        $algorithm = $this->getAlgorithm($algorithmName);

        return $algorithm->compute($params);
    }

    /**
     * @param string $algorithmName
     * @return AlgorithmInterface
     */
    private function getAlgorithm($algorithmName)
    {
        if (!isset($this->algorithmsList[$algorithmName])) {
            throw new AlgorithmNotExists(sprintf(
                'Algorithm %s not defined',
                $algorithmName
            ));
        }

        return $this->algorithmsList[$algorithmName];
    }
}
<?php

namespace AppBundle\Controller;

use AppBundle\Math\Parameters;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MathController extends FOSRestController
{
    /**
     * @Route("/math/{algorithm}", name="Math execution page")
     *
     * @param Request $request
     * @param $algorithm
     * @return Response
     */
    public function indexAction(Request $request, $algorithm)
    {
        try {
            $mathRunner = $this->get('math_runner');

            $parameters = new Parameters();
            $parameters->fromIterator($request->query->getIterator());

            $view = $this->view(
                $mathRunner->run($algorithm, $parameters),
                200
            );
        } catch (\RuntimeException $e) {
            $view = $this->view(
                $e->getMessage(),
                400
            );
        }

        return $this->handleView($view);
    }
}

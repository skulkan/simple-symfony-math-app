<?php

namespace Tests\AppBundle\Math;

use AppBundle\Math\Algorithm\AlgorithmInterface;
use AppBundle\Math\MathAlgorithmRunner;
use AppBundle\Math\Parameters;

class MathAlgorithmRunnerTest extends \PHPUnit_Framework_TestCase
{
    public function testSingleAlgorithmRun()
    {
        $runner = new MathAlgorithmRunner(['test' => $this->createMockForAlgorithmInterface()]);

        $result = $runner->run('test', new Parameters());

        $this->assertEquals([1, 2, 3], $result);
    }

    /**
     * @expectedException \AppBundle\Math\Exception\AlgorithmNotExists
     */
    public function testNotDefinedAlgorithmRun()
    {
        $runner = new MathAlgorithmRunner([]);

        $result = $runner->run('test', new Parameters());

        $this->assertEquals([1, 2, 3], $result);
    }

    /**
     * @expectedException \AppBundle\Math\Exception\AlgorithmInterfaceExpected
     */
    public function testDefineWrongTypedAlgorithm()
    {
        new MathAlgorithmRunner(['test' => new \stdClass()]);
    }

    public function createMockForAlgorithmInterface()
    {
        $mock = $this->getMockBuilder(AlgorithmInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('compute')
            ->willReturn([1, 2, 3]);

        return $mock;
    }
}
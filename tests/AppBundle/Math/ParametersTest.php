<?php

namespace Tests\AppBundle\Math;

use AppBundle\Math\Parameters;

class ParametersTest extends \PHPUnit_Framework_TestCase
{
    public function testAddAndGetParameter()
    {
        $parameters = new Parameters();
        $parameters->addParameter('test', 22);

        $this->assertEquals(22, $parameters->getParameter('test'));
    }

    /**
     * @expectedException \AppBundle\Math\Exception\ParameterNotProvided
     */
    public function testGetNotExistingParameter()
    {
        $parameters = new Parameters();
        $parameters->getParameter('test');
    }

    /**
     * @expectedException \AppBundle\Math\Exception\ParameterAlreadyProvided
     */
    public function testAddSameParameterTwoTimes()
    {
        $parameters = new Parameters();
        $parameters->addParameter('test', 1);
        $parameters->addParameter('test', 2);
    }
}
